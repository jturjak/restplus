from database import db
from database.models import Users

def create_user(data):
    name = data.get('username')
    new_user= Users(username=name)
    query = Users.query.filter_by(username=name).first()
    if query is None:
        db.session.add(new_user)
        db.session.commit()
        return True
    else:
        return False
def delete_user(data):
    name = data.get('username')
    query=Users.query.filter_by(username=name).first()
    if query is None:
        return False
    else:
        db.session.delete(query)
        db.session.commit()
        return True
def update_user(data):
    old_username = data.get('old_username')
    new_username = data.get('new_username')
    query_old = Users.query.filter_by(username = old_username).first()
    if query_old is None:
        return 0
    else:
        query_new = Users.query.filter_by(username = new_username).first()
        if query_new is None:
            query_old.username = new_username
            db.session.add(query_old)
            db.session.commit()
            return 1
        else:
            return -1