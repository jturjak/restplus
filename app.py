from flask import Flask,jsonify,Blueprint, url_for,request
from flask_sqlalchemy import SQLAlchemy
from flask_restplus import Api, Resource, fields
from database import db
from database.models import Users
import os
from methods.methods import create_user,delete_user,update_user

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
api = Api(app)


def configure_app(flask_app):
    flask_app.config['SQLALCHEMY_DATABASE_URI'] =  'sqlite:///' + os.path.join(basedir, 'app.db')
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

def initialize_app(flask_app):
    configure_app(flask_app)
    blueprint = Blueprint('api', __name__, url_prefix='/api')
    flask_app.register_blueprint(blueprint)
    db.init_app(flask_app)
    app.app_context().push()
    db.create_all()

def main():
    initialize_app(app)

    user = api.model('user',{
    'username': fields.String(required=True, description='Username'),
})
    user_update = api.model('update_user',{
    'old_username': fields.String(required=True, description='Old username'),
    'new_username': fields.String(required=True, description='New username'),
})

    @api.route('/user')
    class User(Resource):
        @api.marshal_list_with(user)
        def get(self):
            users = Users.query.all()
            return users


        @api.expect(user)
        @api.response(501, 'Username has to be unique!')
        @api.response(201, 'Added new user successfully!')
        def post(self):
            data = request.json
            if create_user(data):
                return{'result':('Added user {}'.format(data.get('username')))}, 201
            else:
                return{'result':'Username has to be unique!'} ,501 
        @api.expect(user)
        @api.response(501, 'Unknown user')
        @api.response(201, 'User successfully deleted!')
        def delete(self):
            data = request.json
            if delete_user(data):
                return{'result':('User {} deleted'.format(data.get('username')))}, 201
            else:
                return{'result':('Unknown user:{}'.format(data.get('username')))},501 
        @api.expect(user_update)
        @api.response(502, 'Unknown user!')
        @api.response(501, 'Username has to be unique!')
        @api.response(201, 'Username successfully updated!')
        def put(self):
            data = request.json
            if update_user(data) == 1:
                return{'result':('Username {} changed to {}'.format(data.get('old_username'),data.get('new_username')))}, 201
            elif update_user(data) == -1:
                return{'result':'New username has to be unique!'},501
            else:
                return{'result':('Unknown user:{}'.format(data.get('old_username')))},502  
    app.run(debug=True)

if __name__ == "__main__":
    main()





