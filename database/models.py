from datetime import datetime

from database import db

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50),unique = True)

    def __repr__(self):
        return '<Users %r>' % self.username